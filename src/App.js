import React from "react";
import * as THREE from 'three'
import {Canvas, useFrame} from "react-three-fiber";
import {useFBX, MeshReflectorMaterial, Environment, Stage, PresentationControls} from '@react-three/drei'
import './App.css';

function Model(props) {
    const fbx = useFBX('/ArshipModelAnimation_v2.fbx')
    return <primitive object={fbx} dispose={null} {...props} />
}

function App() {
    return (
        <div id={"canvas-container"}>
            <Canvas dpr={[1, 2]} shadows camera={{fov: 45}}>
                <color attach="background" args={['#101010']}/>
                {/*<fog attach="fog" args={['#101010', 10, 20]}/>*/}
                <React.Suspense fallback={null}>
                    <PresentationControls speed={1.5} global zoom={0.7} polar={[-0.1, Math.PI / 4]}>
                        <Stage environment={null} intensity={1} contactShadow={false} shadowBias={-0.0015}>
                            <Model scale={0.01}/>
                        </Stage>
                        <mesh rotation={[-Math.PI / 2, 0, 0]}>
                            <planeGeometry args={[170, 170]}/>
                        </mesh>
                    </PresentationControls>
                </React.Suspense>
            </Canvas>
        </div>
    )
        ;
}

export default App;
